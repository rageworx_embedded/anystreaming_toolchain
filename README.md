# Tool Chains #

* This toolchains a compiler for NXP ASC88xx (Mozart) system.


## configure ##
* Clone or download this project on your Linux system.
* Check 32bit or 64bit of your Linux.
     - Each compilers placed in Bin/32bit or Bin/64bit.
* Copy right version of tool chains to here:
     - Extract tar.bz2 to  "/opt/Mozart_toolcahin"
     - make permission to everybody.
* Modify your "${HOME}/.profile" or "${HOME}/.bashrc"
     - Make full PATH runs it on your shell as like :
~~~
export PATH=/opt/Mozart_toolchain/arm-eabi-uclibc/usr/bin:${PATH}
~~~
* Modify "devel_mozart_eabi" file for your system.
     - copy "devel_mozart_eabi" to your home directory.
     - Make import as a source like this on your .profile or .bashrc
~~~
source ${HOME}/devel_mozart_eabi
~~~

## test ##
* (If you are first time, )Reload your terminal or bash.
* Make a helloworld.c as you known.
* Compile it with this.
~~~
arm-linux-gcc helloworld.c -o helloworld
~~~
* If is it compiles well done, it is OK.

